package com.flappybird;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.TestRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.appium.java_client.AppiumDriver;
import org.apache.commons.lang3.mutable.MutableDouble;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.flappybird.ManageVar;
import com.flappybird.ScreenshotListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class kamcordTest {

  private static AppiumDriver driver;
  public String appFileName="/Users/stathis/Library/Developer/Xcode/DerivedData/Flappy_Felipe-fgkiqlgrxmpkmodathgylhdiiris/Build/Products/Debug-iphoneos/Flappy Felipe.app";
  public String port = "4723";
  private double centerX;
  private double centerY;
  private String title = "Hello world Test";
  private String status_watchreplays = "Watch Replays";
  private String status_uploadfinished = "Upload Finished!";
  
  private void launchApp() throws MalformedURLException
  {
    //Appium setup
      Reporter.log("Setting up Appium",true);
      String current = System.getProperty("user.dir");
      Reporter.log("Current direcotry:"+current,true);
      File app = new File(appFileName);
      Reporter.log("Absolute Path:"+app.getAbsolutePath(),true);
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability("appium-version", "1.0");
      capabilities.setCapability("platformName", "iOS");
      capabilities.setCapability("platformVersion", "8.1");
      capabilities.setCapability("deviceName", "iPad 2");
      capabilities.setCapability("app", app.getAbsolutePath());
      driver = new AppiumDriver(new URL("http://127.0.0.1:"+port+"/wd/hub"), capabilities);
      Reporter.log("Appium setup finished",true);
  }
  @BeforeClass
  public void setup(ITestContext c) throws Exception
  {   
      
      port = ManageVar.getVarString("appiumport", port);
      appFileName = ManageVar.getVarString("apppath", appFileName);
      launchApp();
      TestRunner tr = (TestRunner) c;
      tr.addTestListener(new ScreenshotListener(driver));  
      Dimension dimensions = driver.manage().window().getSize();
      centerX = dimensions.getWidth()/2;
      centerY = dimensions.getHeight()/2;  
  }
  
  @AfterClass
  public void tearDown() throws InterruptedException
  {
      Thread.sleep(5000);
      Reporter.log("TearDown",true);
      driver.quit();
  }
  boolean isGameOver()
  {
      WebElement element = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]"));
      List<WebElement> childs = element.findElements(By.xpath(".//*"));
      if (childs.size() < 3)
      {
          Reporter.log("Game is going on",true);
          return false;
      }
      else
      {
          Reporter.log("Game is Over",true);
          return true;
      }
  }
  void clickClick(double x, double y)
  {
      final MutableDouble xcord = new MutableDouble(x);
      final MutableDouble ycord = new MutableDouble(y);
      Reporter.log("Clicking on "+xcord+" and "+ycord,true);
      HashMap<String, Double> command = new HashMap<String, Double>() 
              {{ put("tapCount", 1.0); put("touchCount", 1.0);put("duration", 1.0); put("x",xcord.toDouble() ); put("y",ycord.toDouble() ); }};
      driver.executeScript("mobile: tap", command);
  }
  @Test(timeOut=120000)
  public void ShareRunFlappy() throws InterruptedException
  { 
      while(!isGameOver())
      {
              clickClick(centerX, centerY);
      }
      
      Reporter.log("Clicking on the Share button with path://UIAApplication[1]/UIAWindow[2]/UIAElement[1]",true);
      WebElement shreButton = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIAElement[1]"));
      clickClick(0.0+shreButton.getLocation().getX(),0.0+shreButton.getLocation().getY());
      Reporter.log("Get the title element xPATH://UIAApplication[1]/UIAWindow[2]/UIATextView[1]",true);
      WebElement titleField = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIATextView[1]"));          
      Reporter.log("Enter text:"+title,true);
      titleField.sendKeys(title+"\\n");
      WebElement shareButton = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIAButton[7]"));
      Reporter.log("Status:"+shareButton.getText(),true);
      shareButton.click();
      
      String status = "";
      while(!status.equals(status_watchreplays) && !status.equals(status_uploadfinished))
      {
          WebElement statusText = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIANavigationBar[1]/UIAStaticText[1]"));
          Reporter.log("Status:"+statusText.getText(),true);
          status = statusText.getText();
      }   
  }
}
